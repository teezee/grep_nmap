#!/usr/bin/Rscript

# Copyright (c) 2016 thomas.zink_at_uni-konstanz_dot_de (tz)
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY. 

# Version History:
# 2016-11-23-01 | tz | initial version

args <- commandArgs(trailingOnly = T);

# check argument; set handle to either file or stdin
if (length(args) == 0) {
  handle <- 'stdin';
} else if (length(args) == 1) {
  handle <- args[1];

  if (!file.exists(handle)) {
    writeLines("file not found");
    quit(save="no", status=1);
  }
}

# read file and set header
greped = read.csv(file=handle, header=F);
colnames(greped) <- c("IP", "Hostname", "Port", "State", "Protocol", "Owner", "Service", "SunRPC", "Version", "Ignored State");

# compute frequencies and order by decreasing frequency
ipfreq <- as.data.frame(table(greped$IP));
ipfreq.ordered <- ipfreq[order(ipfreq$Freq, decreasing=T),]

portfreq <- as.data.frame(table(greped$Port));
portfreq.ordered <- portfreq[order(portfreq$Freq, decreasing=T),]

servicefreq <- as.data.frame(table(greped$Service));
servicefreq.ordered <- servicefreq[order(servicefreq$Freq, decreasing=T),]

# plot to pdfs
pdf(file='top10ports.pdf', width = 10, height = 7);
barplot(portfreq.ordered[seq(10),2], names.arg = portfreq.ordered[seq(10),1], main="Top 10 Ports");

pdf(file='top10ips.pdf', width = 16, height = 7);
barplot(ipfreq.ordered[seq(10),2], names.arg = ipfreq.ordered[seq(10),1], main="Top 10 IPs")

pdf(file='top10services.pdf', width = 12, height = 7);
barplot(servicefreq.ordered[seq(10),2], names.arg = servicefreq.ordered[seq(10),1], main="Top 10 Services")

# if data came from stdin, write the file
if (handle=='stdin') {
  write.table(greped, file="", quote=F, row.names = F, col.names = F, sep=',', na="");
}