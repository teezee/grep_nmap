#!/usr/bin/env python3

'''
grep_nmap.py

Copyright (c) 2016 thomas.zink_at_uni-konstanz_dot_de (tz)	
	
Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY. 

	
Description:
Parses nmap grepable output report and transforms it into csv.
Outputs only hosts that are up.

From nmap man page (man nmap):

"""
Grepable output consists of comments (lines starting with a pound (#)). and target lines.
A target line includes a combination of six labeled fields, separated by tabs and followed with a colon.
The fields are

    Host, Ports, Protocols, Ignored State, OS, Seq Index, IP ID, Status.

The most important of these fields is generally Ports, which gives details on each interesting port.
It is a comma separated list of port entries.
Each port entry represents one interesting port, and takes the form of seven slash (/) separated subfields.
Those subfields are:

    Port number, State, Protocol, Owner, Service, SunRPC info, and Version info.
"""

E.g. the input string

    Host: 45.33.49.119 (ack.nmap.org)	Ports: 22/open/tcp//ssh///, 25/open/tcp//smtp///, 42/closed/tcp//nameserver///	Ignored State: filtered (974)

is transformed into the following output:

    45.33.49.119, ack.nmap.org, 22, open, tcp, , ssh, , , filtered (974)
    45.33.49.119, ack.nmap.org, 25, open, tcp, , smtp, , , filtered (974)
    45.33.49.119, ack.nmap.org, 42, closed, tcp, , nameserver, , , filtered (974)

Usage:
Can be used to parse a nmap grepable output file, or to parse stdin.

Example with output file:

# nmap -T4 -oG nmapgrepable.txt insecure.org
# ./parse_nmap.py nmapgrepable.txt

Example with stdin:

# nmap -T4 -oG - insecure.org | ./parse_nmap.py


Version History:
2016-11-22-01 | tz | fix port parsing in case of buggy nmap output (subfield contains ", ")
2016-11-21-01 | tz | initial version
'''
import sys
import re
import fileinput

__all__ = ["Port", "Host"]


class Port:
    '''
    Represents a port.
    Constructor takes the port representation string as input.
    
    It is a comma separated list of port entries.
    Each port entry represents one interesting port, and takes the form of seven slash (/) separated subfields.
    Those subfields are:
    
        Port number, State, Protocol, Owner, Service, SunRPC info, Version info.
    
    E.g.:
        22/open/tcp//ssh//OpenSSH 6.6.1p1 Ubuntu 2ubuntu2.8 (Ubuntu Linux; protocol 2.0)/
    '''
    def __init__ (self, portrepr):
        # repair nmap output
        portrepr = portrepr.replace(',',';')
        self._array = portrepr.split('/')

    def port(self):
        return self._array[0]

    def state(self):
        return self._array[1]

    def proto(self):
        return self._array[2]

    def owner(self):
        return self._array[3]
        
    def service(self):
        return self._array[4]

    def sunrpc(self):
        return self._array[5]
    
    def version(self):
        return self._array[6]

    '''
    outputs the port in nmap grepable output format
    '''
    def __repr__ (self):
        return '/'.join(self._array).strip()

    '''
    outputs the port in csv format
    '''
    def __str__ (self):
        return self.port() + ", " + self.state() + ", " + self.proto() + ", " +self.owner() + ", " + self.service() + ", " + self.sunrpc()  + ", " + self.version()


class Host:
    '''
    class Host

    Represents the host with all ports.
    Constructor takes the target line output by nmap -oG as input.

    A target line includes a combination of six labeled fields, separated by tabs and followed with a colon.
    The fields are

        Host, Ports, Protocols, Ignored State, OS, Seq Index, IP ID, Status.

    Eg:
        Host: 192.168.2.6 (host6.local)	Ports: 22/open/tcp//ssh///, 443/open/tcp//https///	Ignored State: filtered (998)
    '''

    __ip_pat = re.compile("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
    __port_pat = re.compile(", \d+/")
    
    def __init__ (self,hostrepr):
        fields = hostrepr.split('\t')
        for i in fields:
            key,val = i.split(':',1)
            self.__dict__[key] = val.strip()
            
        self._ip = self.__ip_pat.findall(self.Host)[0]
        self._name = self.Host[self.Host.find('(')+1:self.Host.find(')')]

        # unfortunately nmap grepable can contain the field delimiter ", " in port subfields (e.g. "(Uses VNC, SOAP)")
        # so we need to use regex to split and rebuild port strings (seriously nmap? why?)
        if "Ports" in self.__dict__.keys():
            splits = self.__port_pat.findall(self.Ports)
            splitted = re.split(self.__port_pat, self.Ports)
            for i in range(len(splits)):
                splitted[i+1] = splits[i].lstrip(', ') + splitted[i+1]
            ports = []
            for p in splitted:
                ports.append(Port(p))
            self.Ports = ports

    def IP(self):
        return self._ip

    def Name(self):
        return self._name

    '''
    outputs the host in csv format
    Host, Ports, Protocols, Ignored State, OS, Seq Index, IP ID, Status
    '''
    def __str__ (self):
        s = ''
        eol = "\n"
        if "Ignored State" in self.__dict__.keys():
            eol = ", " + self.__dict__["Ignored State"] + "\n"
        if "Ports" in self.__dict__.keys():
            for p in self.Ports:
                s += self.IP() + ", " + self.Name() + ", " + str(p) + eol
        return s[:-1]

    '''
    outputs the host in nmap grepable output format
    '''
    def __repr__ (self):
        s = "Host: " + self.Host
        eol = ""
        if "Ignored State" in self.__dict__.keys():
            eol = ", " + self.__dict__["Ignored State"]
        if "Ports" in self.__dict__.keys():
            s +=  "\tPorts: "
            for p in self.Ports:
                s += repr(p) + ", "
        s += eol
        return s


def main():
    hosts = []
    for line in fileinput.input():
        if line.find("Ports:") != -1:
            host = Host(line)
            print(str(host))
            hosts.append(host)
    return hosts
    

if __name__ == '__main__':
    hosts = main()
